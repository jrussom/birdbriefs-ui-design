$(document).ready(function() {
	/* If the browser doesn't support SVG, add this class so it can switch out images in the CSS and in DOM */
	if (!supportsSVG()) {
		$('body').addClass('no-svg');
		var imgs = document.getElementsByTagName('img');
		var dotSVG = /.*\.svg$/;
		for (var i = 0; i != imgs.length; ++i) {
			if(imgs[i].src.match(dotSVG)) {
				imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
			}
		}
	}

	var check;
	$("#checkBillSame").on("click", function() {
		check = $("#checkBillSame").is(":checked");
	    if(check) {
	       $('#billingInfo').show();
	    } else {
	       $('#billingInfo').hide();
	    }
	});
});

function supportsSVG() {
    return !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect;
}

function showStep1() {
	//event.preventDefault();
	$('#tickets').show();
	$('#shipping, #review').hide();

	$('#steps li').removeClass('active');
	$('#steps li#step-select').addClass('active');
	$('#steps .bar').removeClass('filled');
}

function showStep2() {
	//event.preventDefault();	
	$('#shipping').show();
	$('#tickets, #review').hide();

	$('#steps li').removeClass('active');
	$('#steps li#step-shipping').addClass('active');

	$('#steps .bar').removeClass('filled');
	$('#steps #bar-toShipping').addClass('filled');
}

function showStep3() {
	if($('input[name="firstName"]').val() == '') {
		$('input[name="firstName"]').parent().addClass('error');
		$('.errorBar').show();
		return false;
	} else { 
		$('#review').show();
		$('#tickets, #shipping').hide();

		$('#steps li').removeClass('active');
		$('#steps li#step-review').addClass('active');

		$('#steps .bar').addClass('filled');
	}
}



